## 377131-rodriguez-conklin 

Exclusive theme build - livermore

## Notes
* HEADER- MODERN TOP BAR *
1. rotator section - these are the rotator images 
2. 2 box image - box sections
3. Rich Text Editor - Accordion Section
4. side by side - accordion image
5. headling section- helpful content
6. 3/4 box section- helpful content
6. lead gen

## History

###**06/21/18** - First commit

###**09/19/18** - theme ready to go live 

###**01/16/19** - Adjusted default settins for Banner Overlay

###**01/21/19** - Added Accodion Alt Title control, and Default topbar button formatting.

###**01/24/19** - Fixed homepage accordion sub-links.

###**06/28/19** - Adjustments homepage overlay and fixed mobile overlay.

###**09/02/19** - Scroll Reveal breaking quiz forms, removed the reveal off the quiz form section.

###**09/08/19** - Mobile Navigation adjustments, Sticky masthead integration update, and topbar adjustments.

###**01/07/20** - Adjustment for Scroll reveal CDN causing CORS Policy issues, and font import adjustment.

###**02/26/20** - Nav Updates, New CSS Only Accordion Added, Sticky Masthead Code Improved, and Removed Last Nav Button formatting on Mobile.

###**05/05/20** - Header update for mobile optimization.
