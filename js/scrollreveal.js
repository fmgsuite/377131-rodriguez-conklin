// scrollreveal (https://github.com/jlmakes/scrollreveal.js)
window.sr = ScrollReveal({
  mobile: false
});


// WELCOME TEXT

sr.reveal('.homepage #section_1 .welcome-text h4', {
  origin: 'bottom',
  distance: '30px',
  duration: 700,
  opacity: 0,
  scale: 1,
  easing: 'ease-in',
  viewFactor: 1
});
sr.reveal('.homepage #section_1 .welcome-text h2 ', {
  origin: 'bottom',
  distance: '30px',
  duration: 500,
  delay: 250,
  opacity: 0,
  scale: 1,
  easing: 'ease-in',
  viewFactor: 1
});
sr.reveal('.homepage #section_1 .welcome-text p', {
  origin: 'bottom',
  distance: '30px',
  duration: 500,
  delay: 500,
  opacity: 0,
  scale: 1,
  easing: 'ease-in',
  viewFactor: 1
});
sr.reveal('.homepage #section_1 div#box1', {
  origin: 'bottom',
  distance: '50px',
  duration: 750,
  delay: 750,
  opacity: 0,
  scale: 1,
  easing: 'ease-in',
  viewFactor: 0.5
});
sr.reveal('.homepage #section_1 div#box2', {
  origin: 'bottom',
  distance: '50px',
  duration: 900,
  delay: 900,
  opacity: 0,
  scale: 1,
  easing: 'ease-in',
  viewFactor: 0.5
});
// Accordion 
sr.reveal('.homepage #section_2 .c-box img', {
  origin: 'bottom',
  distance: '30px',
  duration: 500,
  opacity: 0,
  scale: 1,
  easing: 'ease-in',
  viewFactor: 0.5
});
sr.reveal('.homepage #section_2 .c-box:nth-child(even) ', {
  origin: 'bottom',
  distance: '30px',
  duration: 500,
  opacity: 0,
  scale: 1,
  easing: 'ease-in',
  viewFactor: 0.5
});
sr.reveal('.homepage #section_4 div#box1', {
  origin: 'bottom',
  distance: '50px',
  duration: 500,
  delay: 250,
  opacity: 0,
  scale: 1,
  easing: 'ease-in',
  viewFactor: 0.5
});
sr.reveal('.homepage #section_5 .o-container', {
  origin: 'bottom',
  distance: '50px',
  duration: 500,
  delay: 500,
  opacity: 0,
  scale: 1,
  easing: 'ease-in',
  viewFactor: 0.5
});
// sr.reveal('.homepage #section_5 div#box2', {
//   origin: 'bottom',
//   distance: '50px',
//   duration: 500,
//   delay: 550,
//   opacity: 0,
//   scale: 1,
//   easing: 'ease-in',
//   viewFactor: 1
// });
// sr.reveal('.homepage #section_5 div#box3', {
//   origin: 'bottom',
//   distance: '50px',
//   duration: 500,
//   delay: 600,
//   opacity: 0,
//   scale: 1,
//   easing: 'ease-in',
//   viewFactor: 1
// });
// sr.reveal('.homepage #section_5 div#box4', {
//   origin: 'bottom',
//   distance: '90px',
//   duration: 500,
//   delay: 650,
//   opacity: 0,
//   scale: 1,
//   easing: 'ease-in',
//   viewFactor: 1
// });

// Scroll Reveal Breaks Quiz forms

sr.reveal('.homepage #section_7', {
  origin: 'bottom',
  distance: '30px',
  duration: 500,
  delay: 500,
  opacity: 0,
  scale: 1,
  easing: 'ease-in',
  viewFactor: .5
});
