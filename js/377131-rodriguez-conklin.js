$(document).ready(function () {
  // ------------------------------------------------------------------
  // Fixed Navigation Menu JS
  // ------------------------------------------------------------------

  $(window).scroll(function () {
    if ($(this).scrollTop() >
      ($('.c-topbar').height()) &&
      ($(window).width() >= 992)) {
      $('header').addClass("affix");
    } else {
      $('header').removeClass("affix");
    }
  });
  
  $('#mainNav').insertAfter('header .c-header__container');	

  // ------------------------------------------------------------------
  // Homeapge Content Box Edits
  // ------------------------------------------------------------------

  $('#section_1 .c-grid__collapse--Boxes #box1').before('<div class="welcome"/>');
  $('.welcome-text').appendTo('#section_1 .welcome');
  $('.homepage #box2 .c-section-boxes__inner-box').addClass('content-boxtwo');
  $('.homepage #box3 .c-section-boxes__inner-box').addClass('content-boxthree');
  $('.homepage #box1 .c-section-boxes__inner-box').addClass('content-boxone');
  $('.homepage #box4 .c-section-boxes__inner-box').addClass('content-boxfour');
  $('.homepage .content-boxone .c-btn').append('<i class="fa fa-plus"/>');
  $('.homepage .content-boxtwo .c-btn').append('<i class="fa fa-plus"/>');
  $('.homepage .content-boxthree .c-btn').append('<i class="fa fa-plus"/>');


  // ------------------------------------------------------------------
  // Homepage Accordion
  // ------------------------------------------------------------------

  (function ($) {
    $('.accordion > li:eq(0) a').addClass('active').next().slideDown();

    $('.accordion li > a').click(function (j) {
      var dropDown = $(this).closest('li').find('p');

      $(this).closest('.accordion').find('p').not(dropDown).slideUp();

      if ($(this).hasClass('active')) {
        $(this).removeClass('active');
      } else {
        $(this).closest('.accordion').find('a.active').removeClass('active');
        $(this).addClass('active');
      }

      dropDown.stop(false, true).slideToggle();

      j.preventDefault();
    });
  })(jQuery);

  //var acimg = $('.homepage #section_3 .c-image__crop img').attr('src');
  //$('.accordion-img img').attr('src', acimg);

  // ------------------------------------------------------------------
  // Homepage Form Edits
  // ------------------------------------------------------------------

  $('.homepage .c-form__align--None').removeClass('o-form__wrapper');
  $('.homepage .section_Form').addClass('home-form');

  // placeholder for homepage form
  $('.homepage #Name0').attr('placeholder', 'Name');
  $('.homepage #Email0').attr('placeholder', 'Email');
  $('.homepage #Phone0').attr('placeholder', 'Phone');
  var messageLabel = $('.contactMessage label').text();
  $('.homepage #Message0').attr('placeholder', messageLabel);

  // conditionals
  // var a = document.getElementsByClassName('formGroup');
  var a = $('.home-form .formGroup');
  if (a.length === 2 && $(window).width() > 992) {
    $('.home-form .contact-form .formGroup:nth-child(2)').css({
      'width': '49%',
      'margin': '0 1% 0 0'
    });
    $('.home-form .contact-form .formGroup:nth-child(3)').css({
      'width': '49%',
      'margin': '0 0 0 1%'
    });
  } else if (a.length === 1 && $(window).width() > 992) {
    $('.home-form .contact-form .formGroup').css({
      'width': '100%',
      'margin': '0'
    });
  }

  $('.homepage .section_Form').addClass('home-form');

  // placeholder for homepage form
  $('.homepage #Name0').attr('placeholder', 'Name');
  $('.homepage #Email0').attr('placeholder', 'Email');
  $('.homepage #Phone0').attr('placeholder', 'Phone');
  var messageLabel = $('.contactMessage label').text();
  $('.homepage #Message0').attr('placeholder', messageLabel);

  // conditionals
  // var a = document.getElementsByClassName('formGroup');
  var a = $('.home-form .formGroup');
  if (a.length === 2 && $(window).width() > 992) {
    $('.home-form .contact-form .formGroup:nth-child(2)').css({
      'width': '49%',
      'margin': '0 1% 0 0'
    });
    $('.home-form .contact-form .formGroup:nth-child(3)').css({
      'width': '49%',
      'margin': '0 0 0 1%'
    });
  } else if (a.length === 1 && $(window).width() > 992) {
    $('.home-form .contact-form .formGroup').css({
      'width': '100%',
      'margin': '0'
    });
  }

  // ------------------------------------------------------------------
  // custom subpages - placeholder text for form section
  // ------------------------------------------------------------------

  $('.section_Form .contact-form div').each(function () {
    var label = $(this).find('span').text();
    $(this).find('input, textarea').attr('placeholder', label);
  });

  // ------------------------------------------------------------------
  // Add class to Dev Theme Lead Generation Forms
  // ------------------------------------------------------------------

  $('.c-leadGen-form').parent().parent().parent().parent().parent().addClass('leadGen-form');

  // ------------------------------------------------------------------
  // Body Content Top Padding for Sticky Masthead
  // ------------------------------------------------------------------

  function topPadding() {

    var headerHeight = $('.c-header').outerHeight() - 1;


    /* Set the matchMedia */
    if (window.matchMedia('(min-width: 992px)').matches) {

      /* Changes when we reach the min-width  */
      // Header
      $('.body-container').css('margin-top', headerHeight);
      $('.body-container').css('border-top', '1px solid transparent');

    } else {
      /* Reset for CSS changes. */
      // Header
      $('.body-container').css('margin-top', '0px');
      $('.body-container').css('border-top', 'none');
    }

  };

  $(window).load(topPadding);

  $(window).resize(topPadding);

  // ------------------------------------------------------------------
  // Logo Replacement (If Needed)
  // ------------------------------------------------------------------

  /* var logo = '//fmg-websites-custom.s3.amazonaws.com/YOURBUILDFOLDER/images/log';
   
  $('#agentLogo').attr('src', logo); */
});

if (window.matchMedia('(min-width: 992px)').matches) {
	    $('.homepage #section_2, .homepage #section_3').wrapAll('<div class="superSection" />');
	    var background = $('.homepage #section_3').css('background-image');
	    $('.homepage #section_3').css('background', 'none');
	    $('.homepage #section_3 .overlay').css('background', 'none');
	    $('.homepage .superSection').css('background-image', background);
}


//footer
$('footer .moonlight-contact-information').html('<p>Jarret Rodriguez-Conklin, ChFC&#174;, CIMA&#174; |CA|</p><p class="moonlight-address_block">413 East Foothill Boulevard, Suite 100 <br />San Dimas, CA 91773</p><p class="moonlight-licenses">CA Insurance Lic. # 0F12433</p><p class="moonlight-phone"><a href="tel:9096670587">909-667-0587</a></p><p><a href="mailto:Info@homesteadwealthmgt.com">Info@homesteadwealthmgt.com</a></p>');